import requests
import json
import codecs
import time

from config import *

import Car



def main():
    input = codecs.open(file_path,'r', encoding)
    input = input.read().split(car_tokens[1])
    for i in range(0, len(input)):    
        car_input = input[i]
        my_car = Car(car_input)
        if (my_car.years_issued[1] > 2020):
            my_car.send_to_elma() 
            time.sleep(0.15)
        else:
            my_car.log('Too old: ')

if __name__ =='__main__':
    main()




