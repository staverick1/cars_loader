class Car:
    def __init__(self, input):
        self.brand = input.split(mark_tokens[0])[1].split(mark_tokens[1])[0]
        self.model = input.split(model_tokens[0])[1].split(model_tokens[1])[0]
        self.modification = input.split(modification_tokens[0])[1].split(modification_tokens[1])[0]
        self.years_issued = [input.split(years_issued_tokens[0])[1].split(years_issued_tokens[1])[0],
                            input.split(years_issued_tokens[1])[1].split(years_issued_tokens[2])[0]]
        if self.years_issued[1] == 'по н.в.':
            self.years_issued[1] = 2024
        self.years_issued = [int(self.years_issued[0]), int(self.years_issued[1])]
        self.body_type = input.split(body_type_tokens[0])[1].split(body_type_tokens[1])[0]

    def log(self, input):
        print(input + ' ' +
                self.brand + ' '
              + self.model + ' '
              + self.modification + ' '
              + str(self.years_issued[0]) + ' '
              + str(self.years_issued[1]) + ' '
              + self.body_type)

    def send_to_elma(self):
        headerset = {
            'Authorization': 'Bearer ' + elma_upload_token
        }
        output = json.dumps({
            "context": {
                "__name": self.brand + ' ' + self.model + ' ' + str(self.years_issued[0]) + ' ' + self.body_type,
                "Brand": self.brand,
                "model": self.model,
                "issueYear": self.years_issued[0],
                "retirementYear": self.years_issued[1],
                "bodyType": self.body_type,
                "modification": self.modification,                
              },
            "withEventForceCreate": True
        })
        responce = requests.post(
            url = elma_input_url,
            data = output,
            headers = headerset
        )
        self.log('Found: ')
        if (responce.status_code > 400):
           print('error')
        else:
           print('Sent successfully')
            

